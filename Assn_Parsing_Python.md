# Assignment: Parsing Logs With Python
---

## asciinema link:
---
https://asciinema.org/a/Rpk3Xm196xhLnygRt9Q7Cx8rV

## What the Script Does:
---
First the script imports the OS module so it can interface with the underyling system.  
In this case to get the desired logs. Then we import the regular expression module so  
we can utilize certain expressions for our script. Then the script is told to go through  
certain logs and find a specific IP address, in this case 12.34.56.78. Next it takes  
those matches and copies it to a blank log file in the output directory called  
"parsed_lines.log". It then opens that file and reads it and if it matches the regex  
it prints the output to the console and outputs the file int he desired directory.

## Why We Want to Script to Handle Logs
---
This exercise might have been a little bit of overkill for the concept of using a script  
for analyzing logs. However, the concept is there. In live environments we have access  
to hundreds of logs and those logs can range from the size of nothing to MB or GB of data.  
Utilizing a script to parse through logs to find entries and information we select or set  
parameters for cuts down on time and resources. Work smarter, not harder.

## Why Analyzing Logs is Necessary
---
Analyzing logs is not just necessary but should be mandatory. Almost everything going  
on has a log attached to it and the ability to see what is going on at a certain time  
with any selected service is huge. It can show vulnerabilities, opportunities, and  
help prevent future incidents. In addition, it can also help remediate an incident  
by reverse engineering what happened, when, and from there we can shore up the defenses.
